import React from 'react';
import {
  StyleSheet,
  PanResponder,
  View
} from 'react-native';

class TagIcon extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dragX: 0,
      dragY: 0,
      dragging: false,
      lastTapTime: 0
    };
    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

      onPanResponderGrant: ({timeStamp}, gestureState) => {
        this.setState({
          dragging: true,
          touchStartTime: timeStamp
        });
      },
      onPanResponderMove: (evt, {dx, dy}) => {
        const {zoomScale} = this.props;
        this.setState({dragX: dx / zoomScale, dragY: dy / zoomScale});
      },
      onPanResponderTerminationRequest: (evt, gestureState) => true,
      onPanResponderRelease: ({timeStamp}, gestureState) => {
        const doubleTapWindow = 200;
        const {x,y,onMove,onDelete} = this.props;
        const {dragX, dragY, touchStartTime, lastTapTime} = this.state;
        let newState = {dragX: 0, dragY: 0, dragging: false};
        if ((timeStamp - touchStartTime) < doubleTapWindow) {
          if ((touchStartTime-lastTapTime) < doubleTapWindow) {
            onDelete();
          } else {
            newState.lastTapTime = timeStamp;
          }
        } else {
          onMove(x + dragX, y + dragY);
        }
        this.setState(newState);
      },
    });
  }

  render() {
    const {x,y,zoomScale} = this.props;
    const {dragX, dragY, dragging} = this.state;
    const radius = 40 / zoomScale;
    return (
      <View
        style={[styles.container, {
          width: 2 * radius,
          height: 2 * radius,
          borderRadius: radius,
          left: x + dragX - radius,
          top: y + dragY - radius
        }, dragging && styles.dragging]}
        {...this._panResponder.panHandlers} >
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#4286f4",
    position: "absolute",
    zIndex: 5,
    opacity: 0.8,
    justifyContent: "center",
    alignItems: "center"
  },
  dragging: {
    backgroundColor: "red"
  }
});

export default TagIcon;