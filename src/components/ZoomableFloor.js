import React from 'react';
import { connect } from 'react-redux';
import {
  Image,
  ScrollView,
  StyleSheet,
  TouchableWithoutFeedback,
  Text
} from 'react-native';

import { createTag, updateTag, deleteTag } from '../actions';
import { getFloorByKey } from '../store';

import TagIcon from './TagIcon';

const mapStateToProps = (state, {floorKey}) => getFloorByKey(state, floorKey);

class ZoomableFloor extends React.Component {
  constructor(props) {
    super(props);
    this.state = { zoomScale: 1 }
  }

  _setZoomScale = ({nativeEvent: {zoomScale}}) => this.setState({zoomScale});

  _tagFromTouch = ({nativeEvent: {locationX, locationY}}) => {
    const {dispatch, floorKey} = this.props;
    dispatch(createTag(floorKey, {x: locationX, y: locationY}))
  };

  _moveTag = (tagKey) => (x, y) => {
    const {dispatch, floorKey} = this.props;
    dispatch(updateTag(floorKey, tagKey, {x, y}));
  }

  _deleteTag = (tagKey) => () => {
    const {dispatch, floorKey} = this.props;
    dispatch(deleteTag(floorKey, tagKey));
  }

  render() {
    const tagIcons = this.props.tags.map((t,i) => 
      <TagIcon
        key={i}
        onMove={this._moveTag(i)}
        onDelete={this._deleteTag(i)}
        zoomScale={this.state.zoomScale}
        {...t} />);
    return (
      <ScrollView
        styles={styles.container}
        minimumZoomScale={1}
        maximumZoomScale={10}
        scrollEventThrottle={16}
        onScroll={this._setZoomScale}>
        <TouchableWithoutFeedback
          onLongPress={this._tagFromTouch}
          >
          <Image style={styles.floorplan} source={{uri: this.props.imageUrl}} />
        </TouchableWithoutFeedback>
        {tagIcons}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    height: "100%",
  },
  floorplan: {
    width: 500,
    height: 500,
    resizeMode: "contain"
  }
});

export default connect(mapStateToProps)(ZoomableFloor);