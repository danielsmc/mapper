export default [
  {
    name: "Building One",
    imageUrl: 'https://art.uiowa.edu/sites/art.uiowa.edu/files/VAB_Floor_Plans_3_full.jpg',
    tags: []
  },{
    name: "Building Two",
    imageUrl: "https://wcs.smartdraw.com/floor-plan/img/building-plan-example.png",
    tags: []
  },{
    name: "Building Three",
    imageUrl: "http://www.assembleparkcity.com/wp-content/uploads/2015/04/Assemble-Floorplan-Updated-wTitles.jpg",
    tags: []
  }
];