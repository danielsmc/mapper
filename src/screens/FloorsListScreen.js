import React from 'react';
import { connect } from 'react-redux';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from 'react-native';

const mapStateToProps = ({floors}) => ({
  floors: floors.map((f,i) => ({id: i, key: i.toString(), ...f}))
});

const FloorItem = ({item:{name, imageUrl, id}, navigation}) => (
  <TouchableHighlight onPress={() => navigation.navigate("Map",{ id, name })}>
    <View style={styles.floorItem}>
      <Image
        style={styles.thumbnail}
        source={{uri: imageUrl}}
        />
      <Text>{name}</Text>
    </View>
  </TouchableHighlight>);

class FloorsListScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "Buildings",
    };
  };

  render() {
    const {floors, navigation} = this.props;
    return (
    <View style={styles.container}>
      <FlatList
        data={floors}
        renderItem={({item}) => <FloorItem item={item} navigation={navigation} />}
        />
    </View>
  )};
}

const styles = StyleSheet.create({
  floorItem: {
    backgroundColor: "#fff",
    flexDirection: "row",
    alignItems: "center",
    borderBottomColor: '#999',
    borderBottomWidth: 1,
  },
  thumbnail: {
    width: 150,
    height: 150,
    marginRight: 30,
    resizeMode: "contain"
  },
  container: {
    flex: 1,
  },
});

export default connect(mapStateToProps)(FloorsListScreen)