import React from 'react';
import {
  Image,
  StyleSheet,
  View
} from 'react-native';

import ZoomableFloor from '../components/ZoomableFloor';

class FloorMapScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam("name"),
  });

  render() {
    const key = this.props.navigation.getParam("id");
    return (
      <View style={styles.container}>
        <ZoomableFloor floorKey={key} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: "hidden"
  },
});

export default FloorMapScreen;