import {AsyncStorage} from 'react-native';
import { loadKey } from './actions';

async function init(store, keys) {
  for (key in keys) {
    let stored = await AsyncStorage.getItem(key);
    let val = stored ? JSON.parse(stored) : keys[key];
    store.dispatch(loadKey(key, val));
  }
}


const middleware = store => next => action => {
  let result = next(action);
  const state = store.getState();
  for (key in state) {
    AsyncStorage.setItem(key, JSON.stringify(state[key]));
  }
  return result;
}

export default { init, middleware };