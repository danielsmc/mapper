import React from 'react';
import { Provider } from 'react-redux';
import {
  createStore,
  applyMiddleware
} from 'redux';

import persister from './persister';
import Navigator from './Navigator';
import store from './store'
import floors from './data/floors';

const theStore = createStore(store, applyMiddleware(persister.middleware));

persister.init(theStore, {
  floors: floors
});

const App = () => (
  <Provider store={theStore}>
    <Navigator />
  </Provider>
);

export default App;