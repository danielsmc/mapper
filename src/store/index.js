import { combineReducers } from 'redux';

import {
  LOAD_KEY,
  CREATE_TAG,
  UPDATE_TAG,
  DELETE_TAG
} from '../actions';

const immutablePush = (array, item) => (
  [...array, item]
);

const immutablePop = (array, index) => (
  [...array.slice(0, index), ...array.slice(index + 1)]
);

const immutableSet = (array, index, value) => (
  [
    ...array.slice(0, index),
    value,
    ...array.slice(index + 1)
  ]
);

export const getFloorByKey = (state, key) => (state.floors[key]);

const floors = (state = [], action) => {
    let floor_id, tag_id, tag, floor;
    switch (action.type) {
      case LOAD_KEY:
        if (action.key === "floors") {
          return action.value
        }
      case CREATE_TAG:
        ({ floor_id, tag } = action);
        floor = state[floor_id];
        return immutableSet(state, floor_id,
          {...floor, tags: immutablePush(floor.tags, tag)});
      case UPDATE_TAG:
        ({ floor_id, tag_id, tag } = action);
        floor = state[floor_id];
        return immutableSet(state, floor_id,
          {...floor, tags: immutableSet(floor.tags, tag_id, tag)});
      case DELETE_TAG:
        ({ floor_id, tag_id } = action);
        floor = state[floor_id];
        return immutableSet(state, floor_id,
          {...floor, tags: immutablePop(floor.tags, tag_id)});
      default:
        return state
    }
  };


export default combineReducers({floors})