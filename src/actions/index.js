export const LOAD_KEY = "LOAD_KEY";

export const CREATE_TAG = "CREATE_TAG";
export const UPDATE_TAG = "UPDATE_TAG";
export const DELETE_TAG = "DELETE_TAG";

export const loadKey = (key, value) => ({type: LOAD_KEY, key, value});
export const createTag = (floor_id, tag) => ({type: CREATE_TAG, floor_id, tag});
export const updateTag = (floor_id, tag_id, tag) => ({type: UPDATE_TAG, floor_id, tag_id, tag});
export const deleteTag = (floor_id, tag_id) => ({type: DELETE_TAG, floor_id, tag_id});