import {
  createStackNavigator,
  createAppContainer
} from 'react-navigation';

import FloorsListScreen from './screens/FloorsListScreen';
import FloorMapScreen from './screens/FloorMapScreen';

const FloorsStack = createStackNavigator({
  List: FloorsListScreen,
  Map: FloorMapScreen
});

export default createAppContainer(FloorsStack);